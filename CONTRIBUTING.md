# jenkins-build-executor: Contribution Guide

You are welcome to contribute to this project by either checking out master and pushing your changes.
These changes are required to be documented. Or you can create a fork of this project. Maintenance and
code reviews are then up to you, the fork is required to be public. Useful changes might be migrated to 
the original repository without special notification. However, a note will be left in the changelog,
if changes are migrated from a fork.

Any change made shall be documented and stated with date, version, author and a short description in the
CHANGELOG file. If you are not listed in the AUTHORS file, you are free to append the existing list in it.

The version of this tool has the following format: `<MAJOR>.<MINOR>.<RELEASE>`

For bugfixes or smaller features, please increase the release number in buildsys/version_info.
For larger feature implementations (e.g. support for different solution types or structures, which are backward compatible), please increase the minor build number in buildsys/version_info.
For major changes (e.g. completly restructured handling of command line arguments, which are not backward compatible), please increate the major build number in buildsys/version_info.

Any time, minor or major version is increased, all lower build numbers shall be set to zero.

Any change to the API shall be included in the sphinx-built HTML documentation. 