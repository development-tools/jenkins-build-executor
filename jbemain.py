'''
Created on 17.08.2018

@author: mkirc
'''

import datetime
import os
import sys
import time

import buildsys


assert buildsys.version >= (0, 1, 0)

usage = """Usage:
python jbemain.py <SOLUTION-NAME> <SOLUTION-PUBLICNAME> <SOLUTION-VERSION> <SOLUTION-DIR>

<SOLUTION-NAME>       :  The name of the solution to build.
<SOLUTION-PUBLICNAME> :  The public name of the solution to build. This can differ from the solution name
                         and may be used by tool such as scanners for SonarQube
<SOLUTION-VERSION>    :  The current version of the solution.
<SOLUTION-DIR>        :  The directory in which the solution file is stored. 
                         This is relative to the directory this tool is called from."""

if __name__ == '__main__':
    path = os.path.abspath(__file__)
    dir_path = os.path.dirname(path)
    
    if len(sys.argv) != 5:
        print(usage)
        exit(-2)
    
    solution_name = sys.argv[1]
    solution_public = sys.argv[2]
    solution_version = tuple(sys.argv[3].split('.'))
    solution_dir = sys.argv[4]
    
    abs_solution_dir = os.path.abspath(solution_dir)    
    tools_cfg_file = open(os.path.join(dir_path, 'tools.cfg'), 'r').read()
    config = eval(tools_cfg_file)
    
    process_cfg_file = open(os.path.join(dir_path, 'process.cfg'), 'r').read()
    process_config = eval(process_cfg_file)
    
    solution = buildsys.solutions.Solution(solution_name, solution_public, solution_version, abs_solution_dir, "{}.sln".format(solution_name))

    log_receiver = None 
    try:
        log_receiver = buildsys.loggers.LogReceiver(os.path.join(solution.root_dir, process_config['logfile']))
        log_receiver.open_log(True)
        st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        log_receiver.log("Build process {} started at {}\n".format(process_config['name'],st))
    except Exception as ex:
        print(str(ex))
        sys.exit(-1)
        
    header = "Jenkins Build Executor using buildsys {}.{}.{}".format(*buildsys.version)
    log_receiver.log("{}\n".format(header))
    log_receiver.log("{}\n".format("="*len(header)))    
    log_receiver.log("Absolute solution path: {}\n".format(os.path.join(abs_solution_dir, "{}.sln".format(solution_name))))
    log_receiver.log("Solution version: {}\n".format(solution_version))

    tool_configs_ok = True
    tool_chain = None
    for tool_name in config.keys():
        if tool_name == "Name":
            tool_chain = buildsys.tools.ToolChain(tool_name)
            continue
        tool_spec = config[tool_name]
        tool = buildsys.tools.Tool(**tool_spec)
        if tool.path is None:
            log_receiver.log("{} is not configured properly. Check path configuration or restart shell or agent\n".format(tool.name))
            tool_configs_ok = False
        else:
            tool_chain.add_tool(tool_name, tool)
        
    if tool_configs_ok == False:
        sys.exit(-1)
        
    assembly_files = solution.get_assembly_files()
    for assembly_file in assembly_files:
        assembly = buildsys.assemblies.Assembly(solution, assembly_file)
        assembly.apply_build_number()
    
    log_receiver.close_log()
    
    build_process = buildsys.buildproc.BuildProcess(abs_solution_dir, solution, tool_chain, **process_config)
    if build_process.execute() == False:
        sys.exit(-1)
    
    # exit 0 upon success
    sys.exit(0)
