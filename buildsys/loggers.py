'''
Created on 19.08.2018

@author: mkirc
'''

import os
import sys
import time
import traceback


class LogReceiver():
    def __init__(self, file_path, force_flush=True, mirror_log=True):
        self._file_path = file_path
        self._file = None
        self._force_flush = force_flush
        self.__mirror_log = mirror_log
        self._last_log = time.time()
        
    def open_log(self, delete_existing=False):
        try:            
            if delete_existing == False:
                if os.path.exists(self._file_path):
                    self._file = open(self._file_path,"a")
                else:
                    self._file = open(self._file_path,"w+")
            else:
                self._file = open(self._file_path,"w+")
        except Exception as ex:
            print("Error in open_log: {}".format(str(ex)))
            print(traceback.format_exc())   
    
    def close_log(self):
        if self._file is not None:
            self._file.close()
    
    def log(self, data):
        #print ("Enter log")
        if self._file_path is None or self._file_path == '':
            return
        
        try:
            if self._file is None:
                self.open_log()
            
            self._file.write("{}".format(data))
            if self.__mirror_log:
                print("{}".format(data))
            now = time.time()
            last_log_age = now - self._last_log
            self._last_log = now
            if last_log_age >= 5.0 or self._force_flush == True:
                self._file.flush()
        except Exception as ex:
            print("Error in log: {}".format(str(ex)))
            print(traceback.format_exc())
            sys.exit(-1)
        
    
class LogSender():
    """
    LogSender: 
        This abstract base class can be used to provide data to log
        from a process started during a build process
    """
    def __init__(self):
        self.__receiver = None
        self.__verbose = False
    
    @property
    def verbose(self):
        return self.__verbose
    
    def attach_to_log_receiver(self, receiver, verbose=False):
        self.__receiver = receiver
        self.__verbose = verbose       
        
    def log(self, data):        
        if self.__receiver is not None:
            self.__receiver.log(data)
    
    
