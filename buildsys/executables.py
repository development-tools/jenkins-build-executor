'''
Created on 18.08.2018

@author: mkirc
'''
import abc

class Executable(metaclass=abc.ABCMeta):
    '''
    Class Executable provides a basic abstract API
    used to execute tools or an entire build process 
    '''


    def __init__(self):
        '''
        Constructor
        '''
        self.__log_sender = []
        
    def add_log_sender(self, log_sender):
        """
        This method adds a log sender to this executable. 
        Log senders provide data to log from the output of any process executed.
        See also: :class:`buildsys.loggers.LogSender` 
        """
        if not log_sender in self.__log_sender:
            self.__log_sender.append(log_sender)
        
    @abc.abstractclassmethod
    def execute(self, *args):
        pass
        
    
        