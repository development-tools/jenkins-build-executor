'''
Created on 17.08.2018

@author: mkirc
'''

import os
import sys
import argparse
import re

import buildsys


class Assembly(object):
    """
    Class Assembly provides information about a single assembly in a solution.
    This information is e.g. the assembly version which is reflected in file properties 
    of libraries.
    """
    def __init__(self, solution: buildsys.solutions.Solution, filePath: str):
        self.__solution = solution
        self.__filePath = filePath
        
        self.__assemblyVersionTemplate = "[assembly: AssemblyVersion(\"{}.{}.{}.{}\")]\n"
        self.__assemblyFileVersionTemplate = "[assembly: AssemblyFileVersion(\"{}.{}.{}.{}\")]\n"

        self.__regEx_AssemblyVersion =      r'^\[assembly\:\sAssemblyVersion\("(\d+\.\d+\.\d+\.\d+)"\)\]$'
        self.__regEx_AssemblyFileVersion = r'^\[assembly\:\sAssemblyFileVersion\("(\d+\.\d+\.\d+\.\d+)"\)\]$'

    def __get_version_spec(self,pattern, input_line, verbose):
        match = re.match(pattern, input_line)
        
        if match is not None:
            version = match.group(1).split(".")
            if verbose == True:
                print ("Current version: {}".format(version))
            return version
        else:
            if verbose == True:
                print ("NO VERSION MATCH IN INPUT {}".format(input_line))
            return (1,0,0,0)
        
    def apply_build_number(self,verbose=False):
        """
        Apply solution version number to this assembly
        """
        updatedFileContent = []
        file = open(self.__filePath,"r")
        lines = file.readlines()
        file.close()
    
        if verbose == True:
            print ("INFO FOR {}\n\n".format(self.__filePath))
    
        for line in lines:
            if line.startswith("[assembly: AssemblyVersion"):                    
                line = self.__assemblyVersionTemplate.format(*(self.__solution.version))
                updatedFileContent.append(line)
                if verbose == True:
                    print (line)
            elif line.startswith("[assembly: AssemblyFileVersion"): 
                line = self.__assemblyFileVersionTemplate.format(*(self.__solution.version))
                updatedFileContent.append(line)
                if verbose == True:
                    print (line)
            else:
                updatedFileContent.append(line)
                if verbose == True:
                    print (line)
                
        file = open(self.__filePath,"w")
        file.writelines(updatedFileContent)
        file.close()
        if verbose == True:
            print ("===========================================================================\n\n")
    
    def read_version(self):
        """
        Read current assembly version info line.
        This method will return an empty string, if no version information was found
        """
        file = open(self.__filePath,"r")
        lines = file.readlines()
        file.close()
    
        for line in lines:
            if line.startswith("[assembly: AssemblyVersion"):    
                # [assembly: AssemblyVersion(1.0.0.0)]
                version = self.__get_version_spec(self.__regEx_AssemblyVersion, line,True)
                line = self.__assemblyVersionTemplate.format(*(version))
                return line
            
        return ""
        
