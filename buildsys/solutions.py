'''
Created on 17.08.2018

@author: mkirc
'''
import os

class Solution(object):
    """
    This class represents a solution. 
    A solution contains assemblies which are built. 
    """
    def __init__(self, name: str, public_name: str, version: tuple, rootDir: str, file_name: str):
        self.__name = name
        self.__public_name = public_name
        self.__version = version
        self.__rootDir = rootDir
        self.__file_name = file_name
        if self.__version is None:
            self.__version = (1,0,0,0)
        
    @property
    def name(self):
        """
        The solution's name (e.g. MySolution)
        """
        return self.__name
    
    @property
    def public_name(self):
        return self.__public_name
    
    @property
    def version(self):
        """
        The solutions version as tuple.
        Default is (1,0,0,0)
        """
        return self.__version
    
    @property
    def version_string(self):
        return "{}.{}.{}.{}".format(*self.version)
    
    @property
    def file_name(self):
        """
        The name of the solution file
        """
        return self.__file_name
    
    @property
    def root_dir(self):
        return self.__rootDir
    
    def get_assembly_files(self): 
        assemblies = []       
        for root, dirs, files in os.walk(self.__rootDir, topdown=True):            
            for name in files:
                path = os.path.join(root, name)                
                if (path.endswith("AssemblyInfo.cs")):
                    assemblies.append(path)
        return assemblies