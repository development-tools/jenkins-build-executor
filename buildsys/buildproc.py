'''
Created on 18.08.2018

@author: mkirc
'''

import os
import time
import buildsys


class BuildProcess(buildsys.executables.Executable):
    '''
    The class BuildProcess implements a configurable process
    which can be used to build any project with a defined tool chain.
    '''


    def __init__(self, cwd: str, solution: buildsys.solutions.Solution, toolchain: buildsys.tools.ToolChain, **kwargs):
        '''
        Constructor
        '''
        
        self.__target = solution
        self.__name = None
        self.__log = False
        self.__verbose = False
        self.__logfile = None
        self.__tool_order = None
        self.__solution = solution
        self.__tool_chain = toolchain
        
        for key, value in kwargs.items():
            if key == 'name':
                self.__name = value
            if key == 'log':
                self.__log = value
            if key == 'logfile':
                self.__logfile = os.path.join(self.__solution.root_dir, value)
            if key == 'verbose':
                self.__verbose = value
            if key == 'order':
                self.__tool_order = value 
            if key == 'mirrorlog':
                self.__mirror_log = value                               
        
        if self.__logfile is not None and self.__log == True:
            self.__log_receiver = buildsys.loggers.LogReceiver(self.__logfile, True, self.__mirror_log)
        else :
            self.__log_receiver = buildsys.loggers.LogReceiver(None, False, True)
        
        self.__sys_tool = buildsys.tools.SystemTool(cwd)
    
    def execute(self):
        """
        Execute the build process
        """
        
        start = time.time()
        
        try:
            for tool_id in self.__tool_order.keys():
                tool = None               
                
                tool_name = self.__tool_order[tool_id]['tool']
                
                tool_target = self.__tool_order[tool_id]['target']
                if tool_target == '%SOLUTION.FILE%':                    
                    tool_target = os.path.join(self.__solution.root_dir, self.__solution.file_name)
                    if self.__verbose:
                        self.__log_receiver.log("Setting '{}' as target for {}...\n".format(tool_target, tool_name))
                else:
                    if "'%SOLUTION.FILE%'" in tool_target:
                        tool_target = tool_target.replace("'%SOLUTION.FILE%'", "{}".format(os.path.join(self.__solution.root_dir, self.__solution.file_name)))
                    if "'%SOLUTION.VERSION%'" in tool_target:
                        tool_target = tool_target.replace("'%SOLUTION.VERSION%'", "{}".format(self.__solution.version_string))
                    if "'%SOLUTION.NAME%'" in tool_target:
                        tool_target = tool_target.replace("'%SOLUTION.NAME%'", "{}".format(self.__solution.name))
                    if "'%SOLUTION.ROOT_DIR%'" in tool_target:
                        tool_target = tool_target.replace("'%SOLUTION.ROOT_DIR%'", "{}".format(self.__solution.root_dir))
                    if "'%SOLUTION.PUBLIC_NAME%'" in tool_target:
                        tool_target = tool_target.replace("'%SOLUTION.PUBLIC_NAME%'", "{}".format(self.__solution.public_name))
                        
                    if self.__verbose:
                        self.__log_receiver.log("Setting '{}' as target for {}...\n".format(tool_target, tool_name))
                if tool_name == 'SYS':
                    tool = self.__sys_tool                    
                    
                else:                    
                    tool = self.__tool_chain.get(tool_name)
                    if tool.params is not None:
                        if "'WIN::%SYS.LAST_CWD%'" in tool.params:
                            tool.params = tool.params.replace("'WIN::%SYS.LAST_CWD%'", self.__sys_tool.make_path(self.__sys_tool.last_cwd, "WIN"))
                        elif "'MSYS::%SYS.LAST_CWD%'" in tool.params:
                            tool.params = tool.params.replace("'MSYS::%SYS.LAST_CWD%'", self.__sys_tool.make_path(self.__sys_tool.last_cwd, "MSYS"))
                    
                if tool is not None:
                    tool.attach_to_log_receiver(self.__log_receiver, self.__verbose)           
                    if tool.execute(tool_target) == False:                    
                        return False                
                else:
                    self.__log_receiver.log("Cannot construct command line for tool {}\n".format(tool_name))
                    return False
                
            return True
        except Exception as ex:
            print(str(ex))
        finally:
            end = time.time()
            runtime = (end - start)
            units = "seconds"
            if runtime > 60.0:
                runtime = runtime / 60.0
                units = "minutes"
            self.__log_receiver.log("\n\n{}\n\nBuild process '{}' finished in {} {}.".format("="*40, self.__name, runtime, units))
            self.__log_receiver.close_log()
