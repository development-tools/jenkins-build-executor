'''
Created on 17.08.2018

@author: mkirc
'''

from dataclasses import dataclass
import errno
import os
from subprocess import check_output
import subprocess

import buildsys


@dataclass(init=True)
class Tool (buildsys.executables.Executable, buildsys.loggers.LogSender):
    """
    This class represents properties of a tool which is part of a toolchain.
    All properties of this dataclass are of type string.
    """
    
    # ================== PROPERTIES ================== #
   
    name: str
    """
    The name of the tool
    """    
    
    version: str
    """
    The version of this tool
    """    
    
    path: str
    """
    The installation path of this tool
    """    
    
    executable: str
    """
    The executable name of this tool
    """
    
    params: str
    """
    Parameters passed to the executable. These can be switches or arguments which do not depend on the built configuration.    
    """
    
    def __repr__(self):
        """
        String representation of this tool
        """
        return "{} version {} [{} / {}]".format(self.name, self.version, self.path, self.executable)
    
    def execute(self, *args):
        """
        Execute the configured tool
        """
        tool_target= args[0]
        cmd_line = "\"{}\" {} {}".format(os.path.join(self.path, self.executable), self.params, tool_target)
        if self.params is None:
            cmd_line = "\"{}\" {}".format(os.path.join(self.path, self.executable), tool_target)
        if self.verbose:
            self.log("Executing {} (version in use: {})\n".format(cmd_line, self.version))
                
        
        try:
            output = check_output(cmd_line, shell=True, stderr=subprocess.STDOUT)
            # Windows CMD uses IBM850 endocing by default. Python has to decode output
            # to transform received bytes to a string
            proc_out = output.decode('IBM850')
            proc_lines = proc_out.split('\r\n')
            for line in proc_lines:
                self.log("{}\n".format(line.strip()))
            return True
        except subprocess.CalledProcessError as grepexc:
            # Windows CMD uses IBM850 endocing by default. Python has to decode output
            # to transform received bytes to a string
            output = grepexc.output.decode('IBM850')
            err_code = grepexc.returncode
            self.log("{} failed with error: {}\n\n{}".format(self.name, err_code, output))
            return False
    
class SystemTool(Tool):
    """
    SystemTool represents a built-in tool used to issue system command line calls 'cd'.
    System shall never be included in a tool chain.
    """
    def __init__(self, base_path):
        """
        base_path: str
            Path from with the build system is initially called.
        """
        super(SystemTool, self).__init__("System",None, None, None, None)        
        self.__base_path = base_path
        self.__last_cwd = None
        
    @property
    def last_cwd(self):
        return self.__last_cwd
    
    def make_path(self, path, style):
        """
        Transform a given path to specified style.
        Supported styles are:
        
        WIN
            simply return given path
            
        MSYS
            transform given path into msys style and return it
        """
        
        if style == "MSYS":
            # Win paths start with <DRIVELETTER>:\
            drive_letter = path[0]
            msys_path = "/{}/".format(drive_letter)
            msys_path += path[3:].replace("\\","/")
            return msys_path
        else:
            return style
        
        pass
        
    def execute(self, *args):
        """
        Execute the SYS tool. 
        The command line to execute is passed in as args list with one element from the build process configuration
        (configuration path: 'order'/'order-id'/'target')        
        """
        cmd_line = args[0]
        
        if self.verbose:
            self.log("Executing {}\n".format(cmd_line))
            
        if cmd_line.upper().startswith("CD"):
            
            self.__last_cwd = os.path.join(self.__base_path, cmd_line[3:])
            if self.verbose:
                self.log("Setting last CWD point to {}".format(self.__last_cwd))
            
            try:
                os.chdir(self.__last_cwd)
                return True
            except Exception as ex:
                self.log("{} failed with error: \n{}".format(self.name, ex))
                return False
        
        elif cmd_line.upper().startswith("MKDIR"):
            path_to_create = os.path.join(self.__base_path, cmd_line[6:])
            try:
                os.makedirs(path_to_create)
                return True
            except OSError as e:
                if e.errno != errno.EEXIST:
                    self.log("{} failed with error: \n{}".format(self.name, e))
                    return False
                else:
                    return True
            except Exception as ex:
                self.log("{} failed with error: \n{}".format(self.name, ex))
                return False
        
        else:
            try:
                output = check_output(cmd_line, shell=True, stderr=subprocess.STDOUT)
                proc_out = output.decode('IBM850')
                proc_lines = proc_out.split('\r\n')
                for line in proc_lines:
                    self.log(line)
                return True
            except subprocess.CalledProcessError as grepexc:
                output = grepexc.output.decode('IBM850')
                err_code = grepexc.returncode
                self.log("{} failed with error: {}\n\n{}".format(self.name, err_code, output))
                return False
                
    
import management    
    
class ToolChain(management.managers.Manageable):
    """
    This class collects tool instances.
    
    Tool chains can be configured using a configuration file that contains a python dictionary.
    Another way of configuring a tool chain is to instantiate tools manually using the :class:`.Tool` dataclass.
    """
    def __init__(self, name):
        self.__name = name
        self.__tools = {}
        super(ToolChain, self).__init__()
        
    def add_tool(self, tool_id:str, tool: Tool):
        """
        Add a tool to the tool chain, if it has not yet been added
        """
        if self.__tools.get(tool_id) is None:
            self.__tools[tool_id] = tool
            
    def list_tools(self):
        """
        Get a list of all configured tools as string
        """
        tool_list_str = ""
        for tool_id in self.__tools.keys():
            tool_list_str += str(self.__tools.get(tool_id)) +"\n"
        return tool_list_str
    
    def get(self, tool_id):
        """
        Get a tool by tool_id.        
        """
        return self.__tools.get(tool_id)
    
    @property
    def manageable_configuration(self):
        return self.__tools
    