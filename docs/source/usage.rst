Usage of the JBE API
====================

The JBE API is provided in a single package named ``buildsys``. It contains a number of modules, which are described in the API documentation and can be imported using a single python import statement such as
	
	``import buildsys``
	
Depending on your use case, there are at least two ways of setting up a build process using the JBE API. The most obvious is to manually create and configure tools, assemble them into a tool chain and to execute them in a manually defined order with some parameters you need. The following example would simply define MSBuild as a tool, create a minimal tool chain and a minimal build process description.

.. literalinclude:: use_ex_man.py
	:lines: 1-
	
Another way of achieving the same goal (namely building a solution) is to use configuration files for the tool chain and the build process and to store as many information in them as possible. However, depending on your use case, some information might still have to be passed in as command line arguments to your implementation. The following example uses the same scenario as before, but stores the tool chain and the build process configuration in two files: tools.cfg and process.cfg

The tools.cfg file contains the following python dictionary:

.. literalinclude:: min_tc.cfg
	:lines: 1-
	
The process.cfg file contains the following python dictionary:

.. literalinclude:: min_proc.cfg
	:lines: 1-
	
Provided these files exist, the first example can be changed to:

.. literalinclude:: use_ex_cfg.py
	:lines: 1-
	
If you want to reflect the solution's version in all created binaries (e.g. executables and libraries built in the solution), you can use the Assembly class to do perform this task. Precondition for this task is, that you build a .NET solution.

The following snippet will obtain a list of included assemblies from a solution and apply a specified version number to each of them.

.. literalinclude:: assem_ver_ex.py
	:lines: 1-
	