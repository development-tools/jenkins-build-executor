.. Jenkins Build Executor documentation master file, created by
   sphinx-quickstart on Fri Aug 17 17:55:12 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Jenkins Build Executor's documentation!
==================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents: 
   
   intro
   api
   usage
   config



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`

