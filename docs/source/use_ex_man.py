import os
import sys
import buildsys

"""

This small example assumes that:

	1. MSBuild is installed on your system and it's installation path is accessible
	using the absolute path to it. For simplicity, a shorter path is used compared to usual standard installations.
	
	2. A .NET solution file called "SampleApp.sln" exists in the same path as the build system's main script.

"""

if __name__ == "__main__":
	# get the absolute path of this script
	path = os.path.abspath(__file__)
    dir_path = os.path.dirname(path)


	# Create a tool with name MS Build in version 15. It is located under C:\Tools\MSBuild\bin.
	# The file to be invoked by the build process is MSBuild.exe and parameter /t:Rebuild is passed
	# in to issue a complete rebuild of the target solution	
	msbuild = buildsys.tools.Tool("MS Build","15.0",r"C:\Tools\MSBuild\bin", "MSBuild.exe", "/t:Rebuild")
	
	# Create a tool chain named 'Default Toolchain'
	tool_chain = buildsys.tools.ToolChain("Default Toolchain")
	
	# add a tool and provide an ID to reference the tool
	tool_chain.add_tool("MSBUILD", msbuild)
	
	# Next define the target solution to be built
	solution = buildsys.solutions.Solution("SampleApp","My .NET Sample application", (1,0,0,0), "dir_path", "SampleApp.sln")
	
	# Finally a build process has to be created and configured
	build_proc_config = {
		'name': 'Default',
		'log': True,
		'verbose': True,
		'logfile': 'jbelog.log',
		'mirrorlog': True,
		'order': {
			1: {
				'tool': "MSBUILD",
				'target': "\"'%SOLUTION.FILE%'\""
			}
		}
	}
	build_proc = buildsys.buildproc.BuildProcess(dir_path, solution, tool_chain, **build_proc_config)
	
	# The build process is started by calling it's execute method
	result = build_proc.execute()
	
	# Inform calling process about success or failure by checking the build process result
	# Note that calling processes might check the return code of this script
	# to take appropriate actions. You should exit with code 0 only, if no error has been detected.
	if result == True:
		print("Success.")
		sys.exit(0)
	else:
		print("Failure")
		sys.exit(-1)