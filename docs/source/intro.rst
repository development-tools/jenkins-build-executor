The Jenkins Build Executor
==========================

Some build processes require many steps to be executed in order. Each configured step calls specific tools, produces output and may be successfull or not.

Executing tools in a Windows batch sometimes shows errors on the console, but Jenkins does not recognize these errors. Either because processes are spawned too fast, or return codes are not handled properly.

The Jenkins Build Executor (JBE) aims to solve these issues by providing a configurable system to realize build processes. Any tool invoked will be invoked as a process in order to be able to react on return code or to scan the output for error indicators.

This tool can be invoked using a simple call to python with the main python file as first argument and trailing application command line arguments.

For example you could invoke the Jenkins Build Executor like this:

.. literalinclude:: example_call.bat
	:lines: 1
	
Assuming you did a checkout to C:\workspace\MySolution\ and the sources are located in sub directory Sources whereas scripts are located in sub directory scripts. Starting from path C:\workspace\MySolution\, the above call would start the JBE in scripts directory, sets the solution name argument to "MySolution", the version to "1.0.0.0" and the solution directory to sub directory Souces. 