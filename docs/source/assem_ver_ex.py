assembly_files = solution.get_assembly_files()
for assembly_file in assembly_files:
	assembly = buildsys.assemblies.Assembly(solution, assembly_file)
	assembly.apply_build_number()