import os
import sys
import buildsys

"""

This small example assumes that:

	1. MSBuild is installed on your system and it's installation path is accessible
	using the absolute path to it. For simplicity, a shorter path is used compared to usual standard installations.
	
	2. A .NET solution file called "SampleApp.sln" exists in the same path as the build system's main script.

"""

if __name__ == "__main__":
	# get the absolute path of this script
	path = os.path.abspath(__file__)
	dir_path = os.path.dirname(path)


	# Read configuration files
	tools_cfg_file = open(os.path.join(dir_path, 'tools.cfg'), 'r').read()
    config = eval(tools_cfg_file)
    
    process_cfg_file = open(os.path.join(dir_path, 'process.cfg'), 'r').read()
    process_config = eval(process_cfg_file)

	# Create the tool chain based on the configuration just loaded
	tool_chain = None
	tool_configs_ok = True
	for tool_name in config.keys():
		if tool_name == "Name":
			tool_chain = buildsys.tools.ToolChain(tool_name)
			continue
		tool_spec = config[tool_name]
		tool = buildsys.tools.Tool(**tool_spec)
		if tool.path is None:
			print("{} is not configured properly. Check path configuration or restart shell or agent\n".format(tool.name))
			tool_configs_ok = False
		else:
		tool_chain.add_tool(tool_name, tool)
	# Exit upon configuration error
	if tool_configs_ok == False:
		sys.exit(-1)
		
	# Next define the target solution to be built
	solution = buildsys.solutions.Solution("SampleApp","My .NET Sample application", (1,0,0,0), "dir_path", "SampleApp.sln")
	
	
	build_proc = buildsys.buildproc.BuildProcess(dir_path, solution, tool_chain, **build_proc_config)
	
	# The build process is started by calling it's execute method
	result = build_proc.execute()
	
	# Inform calling process about success or failure by checking the build process result
	# Note that calling processes might check the return code of this script
	# to take appropriate actions. You should exit with code 0 only, if no error has been detected.
	if result == True:
		print("Success.")
		sys.exit(0)
	else:
		print("Failure")
		sys.exit(-1)
	
	