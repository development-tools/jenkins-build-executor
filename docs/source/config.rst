Writing configurations for the build system
===========================================

The buildsystem 'Jenkins Build Executor' (JBE) can be configured with two configuration files and a minimal number of command line arguments. One configuration file describes a tool chain which is used to build a project. Tool chains are a set of tools, specified by a name, an installation path, the name of the corresponding executable file and a set of parameters passed to them during the build process. The following example configures a tool chain which contains three tools.

.. literalinclude:: tools.cfg
	:lines: 1-
	
Next to the tool chain configuration file, another file has to exist, which describes the build process itself. In this file, logging options are set and the execution order of tools, defined in a tool chain, is defined.

.. literalinclude:: process.cfg
	:lines: 1-
	
Definition of tools
-------------------

As already mentioned above, tools are defined by a set of properties. These are:

name
	A descriptive name for a tool. This property is used in log files.
	
version
	The version of a tool. This property is used in log files.
	
path
	This property defines the installation path of a tool. Paths can be defined as absolute path or by using an environmental variable (see example above).
	
executable
	The property 'executable' specifies the filename of the tool. 
	
params
	The property 'params' defines a list or parameters passed to the tool when it is executed.
	
Within a tool chain, tools are listed under a key. This key is used to reference tools. *Name* is a reserved key and specifies the name of a tool chain.

.. literalinclude:: tools.cfg
	:lines: 3-8
	
The above snippet defines a tool named *MSBuild* in version 15. It's installation path is defined by an environmental variable called *MSBUILD_HOME*. When the tool is executed, a file named *MSBuild.exe* will be invoked in the specified path and parameters */t:Rebuild /p:Configuration=Release* are passed in. The tool can be referenced as *MSBUILD*.


Referencing tools in a build process
------------------------------------

The configuration of a build process uses a similar syntax as tool chain configurations. The following properties are defined:

name
	The name of the described build process. This is used for logging.
	
log
	This flag defines, if logging is enabled for this build process. Valid values are True and False.
	
verbose
	The verbose flag defines, how detailed the resulting log file will be. If set to False, only output of called tools is included in the log file. If set to True, tool configurations, build process configurations and generated command lines are included in addition to the normal build process output.
	
logfile
	The logfile property defines the name of the output log file is defined. This file will be placed next to the main build system script (jbemain.py)
	
mirrorlog
	This flag defines, if log entries are mirrored to standard output. If set to True, any log entry will also be printed to standard output. If set to False, log entries are written to log file only.
	
order
	Property order is used to describe the order in which configured tools are executed. Each entry in the order configuration references a tool and defines a target. Keys in the order configuration should represent numerical values in increasing order. 
	
Entries in the property owner use the following properties:

tool
	The identification name of a tool as defined in a tool chain
	
target
	The target file for the called tool. %SOLUTION.FILE% is a special notation and will be replaced by the system with the proper filename of the solution which is built. This is important when building .NET solutions.
	
.. literalinclude:: process.cfg
	:lines: 7-10
	
The above snippet references a tool identifies as *NUGET* and passes in the solution file as target argument. Using the generic %SOLUTION.FILE% notation, such an element can be reused in other build process configuration files.	
	 
	 
Built-in variables for usage in build process configurations
------------------------------------------------------------

The internal build process execution routine contains a pre-processor which can substitute a set of variables with configuration values. Variables can be accessed using a string in the form ``'%<VARIABLE-NAME>%'``. Note that single quotes are required. 

The following variables are defined:

SOLUTION.NAME
	This variable holds the name of the solution. Via this name, the corresponding solution file (file extension: .sln) can be found in the file system.
	
SOLUTION.PUBLIC_NAME
	This variable holds the public name of the solution. Configuration is done via command line arguments when the build system is invoked. The public name of a solution can be used by external tools such as scanners for SonarQube.
	
SOLUTION.VERSION
	This variable holds the version information of the solution. Configuration is done via command line arguments when the build system is invoked.
	
SOLUTION.ROOT_DIR
	This variable holds the path in which the solution file can be found.
	
SOLUTION.FILE
	This variable holds the filename of the solution file.