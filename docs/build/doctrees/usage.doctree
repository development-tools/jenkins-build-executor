��YF      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�h �section���)��}�(hhh]�(h �title���)��}�(h�Usage of the JBE API�h]�h �Text����Usage of the JBE API�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�bD:\My\Projects\Python\buildtools\JenkinsBuildExecutor\jenkins-build-executor\docs\source\usage.rst�hKubh �	paragraph���)��}�(h��The JBE API is provided in a single package named ``buildsys``. It contains a number of modules, which are described in the API documentation and can be imported using a single python import statement such as�h]�(h�2The JBE API is provided in a single package named �����}�(h�2The JBE API is provided in a single package named �hh-hhhNhNubh �literal���)��}�(h�``buildsys``�h]�h�buildsys�����}�(hhhh8ubah}�(h]�h!]�h#]�h%]�h']�uh)h6hh-ubh��. It contains a number of modules, which are described in the API documentation and can be imported using a single python import statement such as�����}�(h��. It contains a number of modules, which are described in the API documentation and can be imported using a single python import statement such as�hh-hhhNhNubeh}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh �block_quote���)��}�(hhh]�h,)��}�(h�``import buildsys``�h]�h7)��}�(hhXh]�h�import buildsys�����}�(hhhhZubah}�(h]�h!]�h#]�h%]�h']�uh)h6hhVubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhSubah}�(h]�h!]�h#]�h%]�h']�uh)hQhhhhhh*hNubh,)��}�(hX�  Depending on your use case, there are at least two ways of setting up a build process using the JBE API. The most obvious is to manually create and configure tools, assemble them into a tool chain and to execute them in a manually defined order with some parameters you need. The following example would simply define MSBuild as a tool, create a minimal tool chain and a minimal build process description.�h]�hX�  Depending on your use case, there are at least two ways of setting up a build process using the JBE API. The most obvious is to manually create and configure tools, assemble them into a tool chain and to execute them in a manually defined order with some parameters you need. The following example would simply define MSBuild as a tool, create a minimal tool chain and a minimal build process description.�����}�(hhuhhshhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh �literal_block���)��}�(hXf  import os
import sys
import buildsys

"""

This small example assumes that:

	1. MSBuild is installed on your system and it's installation path is accessible
	using the absolute path to it. For simplicity, a shorter path is used compared to usual standard installations.
	
	2. A .NET solution file called "SampleApp.sln" exists in the same path as the build system's main script.

"""

if __name__ == "__main__":
	# get the absolute path of this script
	path = os.path.abspath(__file__)
    dir_path = os.path.dirname(path)


	# Create a tool with name MS Build in version 15. It is located under C:\Tools\MSBuild\bin.
	# The file to be invoked by the build process is MSBuild.exe and parameter /t:Rebuild is passed
	# in to issue a complete rebuild of the target solution	
	msbuild = buildsys.tools.Tool("MS Build","15.0",r"C:\Tools\MSBuild\bin", "MSBuild.exe", "/t:Rebuild")
	
	# Create a tool chain named 'Default Toolchain'
	tool_chain = buildsys.tools.ToolChain("Default Toolchain")
	
	# add a tool and provide an ID to reference the tool
	tool_chain.add_tool("MSBUILD", msbuild)
	
	# Next define the target solution to be built
	solution = buildsys.solutions.Solution("SampleApp","My .NET Sample application", (1,0,0,0), "dir_path", "SampleApp.sln")
	
	# Finally a build process has to be created and configured
	build_proc_config = {
		'name': 'Default',
		'log': True,
		'verbose': True,
		'logfile': 'jbelog.log',
		'mirrorlog': True,
		'order': {
			1: {
				'tool': "MSBUILD",
				'target': "\"'%SOLUTION.FILE%'\""
			}
		}
	}
	build_proc = buildsys.buildproc.BuildProcess(dir_path, solution, tool_chain, **build_proc_config)
	
	# The build process is started by calling it's execute method
	result = build_proc.execute()
	
	# Inform calling process about success or failure by checking the build process result
	# Note that calling processes might check the return code of this script
	# to take appropriate actions. You should exit with code 0 only, if no error has been detected.
	if result == True:
		print("Success.")
		sys.exit(0)
	else:
		print("Failure")
		sys.exit(-1)�h]�hXf  import os
import sys
import buildsys

"""

This small example assumes that:

	1. MSBuild is installed on your system and it's installation path is accessible
	using the absolute path to it. For simplicity, a shorter path is used compared to usual standard installations.
	
	2. A .NET solution file called "SampleApp.sln" exists in the same path as the build system's main script.

"""

if __name__ == "__main__":
	# get the absolute path of this script
	path = os.path.abspath(__file__)
    dir_path = os.path.dirname(path)


	# Create a tool with name MS Build in version 15. It is located under C:\Tools\MSBuild\bin.
	# The file to be invoked by the build process is MSBuild.exe and parameter /t:Rebuild is passed
	# in to issue a complete rebuild of the target solution	
	msbuild = buildsys.tools.Tool("MS Build","15.0",r"C:\Tools\MSBuild\bin", "MSBuild.exe", "/t:Rebuild")
	
	# Create a tool chain named 'Default Toolchain'
	tool_chain = buildsys.tools.ToolChain("Default Toolchain")
	
	# add a tool and provide an ID to reference the tool
	tool_chain.add_tool("MSBUILD", msbuild)
	
	# Next define the target solution to be built
	solution = buildsys.solutions.Solution("SampleApp","My .NET Sample application", (1,0,0,0), "dir_path", "SampleApp.sln")
	
	# Finally a build process has to be created and configured
	build_proc_config = {
		'name': 'Default',
		'log': True,
		'verbose': True,
		'logfile': 'jbelog.log',
		'mirrorlog': True,
		'order': {
			1: {
				'tool': "MSBUILD",
				'target': "\"'%SOLUTION.FILE%'\""
			}
		}
	}
	build_proc = buildsys.buildproc.BuildProcess(dir_path, solution, tool_chain, **build_proc_config)
	
	# The build process is started by calling it's execute method
	result = build_proc.execute()
	
	# Inform calling process about success or failure by checking the build process result
	# Note that calling processes might check the return code of this script
	# to take appropriate actions. You should exit with code 0 only, if no error has been detected.
	if result == True:
		print("Success.")
		sys.exit(0)
	else:
		print("Failure")
		sys.exit(-1)�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']��source��fD:\My\Projects\Python\buildtools\JenkinsBuildExecutor\jenkins-build-executor\docs\source\use_ex_man.py��	xml:space��preserve��linenos���highlight_args�}��linenostart�Ksuh)h�hh*hK
hhhhubh,)��}�(hX�  Another way of achieving the same goal (namely building a solution) is to use configuration files for the tool chain and the build process and to store as many information in them as possible. However, depending on your use case, some information might still have to be passed in as command line arguments to your implementation. The following example uses the same scenario as before, but stores the tool chain and the build process configuration in two files: tools.cfg and process.cfg�h]�hX�  Another way of achieving the same goal (namely building a solution) is to use configuration files for the tool chain and the build process and to store as many information in them as possible. However, depending on your use case, some information might still have to be passed in as command line arguments to your implementation. The following example uses the same scenario as before, but stores the tool chain and the build process configuration in two files: tools.cfg and process.cfg�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh,)��}�(h�<The tools.cfg file contains the following python dictionary:�h]�h�<The tools.cfg file contains the following python dictionary:�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh�)��}�(h��{
	'Name': ".NET toolchain",	
	'MSBUILD':{
		'name': "MSBuild",
		'version': "15.0",
		'path': r",
		'executable': "C:\Tools\MSBuild\bin",
		'params': "/t:Rebuild"		
	},
}�h]�h��{
	'Name': ".NET toolchain",	
	'MSBUILD':{
		'name': "MSBuild",
		'version': "15.0",
		'path': r",
		'executable': "C:\Tools\MSBuild\bin",
		'params': "/t:Rebuild"		
	},
}�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']��source��cD:\My\Projects\Python\buildtools\JenkinsBuildExecutor\jenkins-build-executor\docs\source\min_tc.cfg�h�h�h��h�}�h�Ksuh)h�hh*hKhhhhubh,)��}�(h�>The process.cfg file contains the following python dictionary:�h]�h�>The process.cfg file contains the following python dictionary:�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh�)��}�(h��{
	'name': 'Default',
	'log': True,
	'verbose': True,
	'logfile': 'jbelog.log',
	'mirrorlog': True,
	'order': {
		1: {
			'tool': "MSBUILD",
			'target': "\"'%SOLUTION.FILE%'\""
		}
	}
}�h]�h��{
	'name': 'Default',
	'log': True,
	'verbose': True,
	'logfile': 'jbelog.log',
	'mirrorlog': True,
	'order': {
		1: {
			'tool': "MSBUILD",
			'target': "\"'%SOLUTION.FILE%'\""
		}
	}
}�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']��source��eD:\My\Projects\Python\buildtools\JenkinsBuildExecutor\jenkins-build-executor\docs\source\min_proc.cfg�h�h�h��h�}�h�Ksuh)h�hh*hKhhhhubh,)��}�(h�@Provided these files exist, the first example can be changed to:�h]�h�@Provided these files exist, the first example can be changed to:�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh�)��}�(hX  import os
import sys
import buildsys

"""

This small example assumes that:

	1. MSBuild is installed on your system and it's installation path is accessible
	using the absolute path to it. For simplicity, a shorter path is used compared to usual standard installations.
	
	2. A .NET solution file called "SampleApp.sln" exists in the same path as the build system's main script.

"""

if __name__ == "__main__":
	# get the absolute path of this script
	path = os.path.abspath(__file__)
	dir_path = os.path.dirname(path)


	# Read configuration files
	tools_cfg_file = open(os.path.join(dir_path, 'tools.cfg'), 'r').read()
    config = eval(tools_cfg_file)
    
    process_cfg_file = open(os.path.join(dir_path, 'process.cfg'), 'r').read()
    process_config = eval(process_cfg_file)

	# Create the tool chain based on the configuration just loaded
	tool_chain = None
	tool_configs_ok = True
	for tool_name in config.keys():
		if tool_name == "Name":
			tool_chain = buildsys.tools.ToolChain(tool_name)
			continue
		tool_spec = config[tool_name]
		tool = buildsys.tools.Tool(**tool_spec)
		if tool.path is None:
			print("{} is not configured properly. Check path configuration or restart shell or agent\n".format(tool.name))
			tool_configs_ok = False
		else:
		tool_chain.add_tool(tool_name, tool)
	# Exit upon configuration error
	if tool_configs_ok == False:
		sys.exit(-1)
		
	# Next define the target solution to be built
	solution = buildsys.solutions.Solution("SampleApp","My .NET Sample application", (1,0,0,0), "dir_path", "SampleApp.sln")
	
	
	build_proc = buildsys.buildproc.BuildProcess(dir_path, solution, tool_chain, **build_proc_config)
	
	# The build process is started by calling it's execute method
	result = build_proc.execute()
	
	# Inform calling process about success or failure by checking the build process result
	# Note that calling processes might check the return code of this script
	# to take appropriate actions. You should exit with code 0 only, if no error has been detected.
	if result == True:
		print("Success.")
		sys.exit(0)
	else:
		print("Failure")
		sys.exit(-1)
	
	�h]�hX  import os
import sys
import buildsys

"""

This small example assumes that:

	1. MSBuild is installed on your system and it's installation path is accessible
	using the absolute path to it. For simplicity, a shorter path is used compared to usual standard installations.
	
	2. A .NET solution file called "SampleApp.sln" exists in the same path as the build system's main script.

"""

if __name__ == "__main__":
	# get the absolute path of this script
	path = os.path.abspath(__file__)
	dir_path = os.path.dirname(path)


	# Read configuration files
	tools_cfg_file = open(os.path.join(dir_path, 'tools.cfg'), 'r').read()
    config = eval(tools_cfg_file)
    
    process_cfg_file = open(os.path.join(dir_path, 'process.cfg'), 'r').read()
    process_config = eval(process_cfg_file)

	# Create the tool chain based on the configuration just loaded
	tool_chain = None
	tool_configs_ok = True
	for tool_name in config.keys():
		if tool_name == "Name":
			tool_chain = buildsys.tools.ToolChain(tool_name)
			continue
		tool_spec = config[tool_name]
		tool = buildsys.tools.Tool(**tool_spec)
		if tool.path is None:
			print("{} is not configured properly. Check path configuration or restart shell or agent\n".format(tool.name))
			tool_configs_ok = False
		else:
		tool_chain.add_tool(tool_name, tool)
	# Exit upon configuration error
	if tool_configs_ok == False:
		sys.exit(-1)
		
	# Next define the target solution to be built
	solution = buildsys.solutions.Solution("SampleApp","My .NET Sample application", (1,0,0,0), "dir_path", "SampleApp.sln")
	
	
	build_proc = buildsys.buildproc.BuildProcess(dir_path, solution, tool_chain, **build_proc_config)
	
	# The build process is started by calling it's execute method
	result = build_proc.execute()
	
	# Inform calling process about success or failure by checking the build process result
	# Note that calling processes might check the return code of this script
	# to take appropriate actions. You should exit with code 0 only, if no error has been detected.
	if result == True:
		print("Success.")
		sys.exit(0)
	else:
		print("Failure")
		sys.exit(-1)
	
	�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']��source��fD:\My\Projects\Python\buildtools\JenkinsBuildExecutor\jenkins-build-executor\docs\source\use_ex_cfg.py�h�h�h��h�}�h�Ksuh)h�hh*hKhhhhubh,)��}�(h��If you want to reflect the solution's version in all created binaries (e.g. executables and libraries built in the solution), you can use the Assembly class to do perform this task. Precondition for this task is, that you build a .NET solution.�h]�h��If you want to reflect the solution’s version in all created binaries (e.g. executables and libraries built in the solution), you can use the Assembly class to do perform this task. Precondition for this task is, that you build a .NET solution.�����}�(hj  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hKhhhhubh,)��}�(h��The following snippet will obtain a list of included assemblies from a solution and apply a specified version number to each of them.�h]�h��The following snippet will obtain a list of included assemblies from a solution and apply a specified version number to each of them.�����}�(hj  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h+hh*hK hhhhubh�)��}�(h��assembly_files = solution.get_assembly_files()
for assembly_file in assembly_files:
	assembly = buildsys.assemblies.Assembly(solution, assembly_file)
	assembly.apply_build_number()�h]�h��assembly_files = solution.get_assembly_files()
for assembly_file in assembly_files:
	assembly = buildsys.assemblies.Assembly(solution, assembly_file)
	assembly.apply_build_number()�����}�(hhhj   ubah}�(h]�h!]�h#]�h%]�h']��source��hD:\My\Projects\Python\buildtools\JenkinsBuildExecutor\jenkins-build-executor\docs\source\assem_ver_ex.py�h�h�h��h�}�h�Ksuh)h�hh*hK"hhhhubeh}�(h]��usage-of-the-jbe-api�ah!]�h#]��usage of the jbe api�ah%]�h']�uh)h	hhhhhh*hKubah}�(h]�h!]�h#]�h%]�h']��source�h*uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j\  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h*�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�j6  j3  s�	nametypes�}�j6  Nsh}�j3  hs�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhhub.