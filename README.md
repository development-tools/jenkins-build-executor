# jenkins-build-executor

This project was originally hosted on https://gitlab.com/development-tools/jenkins-build-executor


Python tool to configure and run .NET build jobs.

jenkins-build-executor currently requires the current directory structure for .NET solutions:

`<ROOTDIR>	(e.g. Sources)
	<ASSEMBLYDIR-1>
	<ASSEMBLYDIR-2>
	...
	<ASSEMBLYDIR-N>
	<SOLUTIONNAME>.sln`
	
A call to `python <PATH-TO-JBEMAIN.PY> <ROOTDIR>` from a directory next to `<ROOTDIR>` will set the 
base path for the build system to the absolute path to `<ROOTDIR>` .

Users of this tool should be familiar with Python with special focus on dictionaries and string notation. 